from solution import first_non_consecutive


def test_1():
    assert first_non_consecutive([1, 2, 3, 4, 6, 7, 8]) == 6


def tests_2():
    assert first_non_consecutive([1, 2, 3, 4, 5, 6, 7, 8]) is None


def tests_3():
    assert first_non_consecutive([4, 6, 7, 8, 9, 11]) == 6


def tests_4():
    assert first_non_consecutive([4, 5, 6, 7, 8, 9, 11]) == 11


def tests_5():
    assert first_non_consecutive([31, 32]) is None


def tests_6():
    assert first_non_consecutive([-3, -2, 0, 1]) == 0


def tests_7():
    assert first_non_consecutive([-5, -4, -3, -1]) == -1
